package com.floatingbtn.com;

import android.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.xml.sax.helpers.LocatorImpl;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{

    Button btn1;
    CheckBox chkwhatsapp,chkhike,chkmessanger;
    RelativeLayout whatsapp,hike,messanger;
    public static String ASSECCEBILITY_SERVICE_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        btn1=(Button)findViewById(R.id.btn_start);
        chkwhatsapp=(CheckBox)findViewById(R.id.chk_whatsapp);
        chkhike=(CheckBox)findViewById(R.id.chk_hike);
        chkmessanger=(CheckBox)findViewById(R.id.chk_fbmessanger);
        whatsapp=(RelativeLayout)findViewById(R.id.container_whatsapp);
        hike=(RelativeLayout)findViewById(R.id.container_hike);
        messanger=(RelativeLayout)findViewById(R.id.container_fbmessanger);
        ASSECCEBILITY_SERVICE_NAME="com.floatingbtn.com/com.floatingbtn.com.FloatbuttonService";



        if(!appInstalledOrNot("com.facebook.orca")){
            messanger.setVisibility(View.GONE);
        }
        if(!appInstalledOrNot("com.bsb.hike")){
            hike.setVisibility(View.GONE);
        }
        if(!appInstalledOrNot("com.whatsapp")){
            whatsapp.setVisibility(View.GONE);
        }
        Boolean iswa_checked=pref.getBoolean("whatsapp", false);
        Boolean ish_checked=pref.getBoolean("hike",false);
        if(iswa_checked){
            chkwhatsapp.setChecked(true);
        }
        else{
            chkwhatsapp.setChecked(false);
        }
        if(ish_checked){
            chkhike.setChecked(true);
        }
        else {
            chkhike.setChecked(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, 1);
                        }
        }
        if(EasyPermissions.hasPermissions(getApplicationContext(),android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
         //   SaveImage(bitmap, name[0]);//function which stores image in sdcard
        }else
        {
            EasyPermissions.requestPermissions(this, "Request for the use of External storage.",
                    123, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        chkwhatsapp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //check if accssebility service is on or not on any check changed events like checked or unchecked in both
//                if(!isAccessebilityServiceEnabled()){
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (!Settings.canDrawOverlays(getApplicationContext())) {
//                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                                    Uri.parse("package:" + getPackageName()));
//                            startActivityForResult(intent, 1);
//                        }
//                    }
//                    Intent callGPSSettingIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//                    startActivity(callGPSSettingIntent);
//                    if (isChecked) {
//                        Log.i("checked", "whatsApp");
//                        editor.putBoolean("whatsapp", true);
////                        editor.putString("whatsapp", "com.whatsapp");
//                        editor.commit();
//                    } else {
//                        Log.i("checkednot", "whatsApp");
////                        editor.putString("whatsapp", "null");
//                        editor.putBoolean("whatsapp", false);
//                        editor.commit();
//                    }
//                }
//                else {
                    if (isChecked) {
                        if(!isAccessebilityServiceEnabled()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (!Settings.canDrawOverlays(getApplicationContext())) {
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                            Uri.parse("package:" + getPackageName()));
                                    startActivityForResult(intent, 1);
                                }
                            }
                            Intent callGPSSettingIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                        Log.i("checked", "whatsApp");
                        editor.putBoolean("whatsapp", true);
//                        editor.putString("whatsapp", "com.whatsapp");
                        editor.commit();
                    } else {
                        Log.i("checkednot", "whatsApp");
//                        editor.putString("whatsapp", "null");
                        editor.putBoolean("whatsapp", false);
                        editor.commit();
                    }
//                }
            }
        });
        chkhike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        if(!isAccessebilityServiceEnabled()) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (!Settings.canDrawOverlays(getApplicationContext())) {
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                            Uri.parse("package:" + getPackageName()));
                                    startActivityForResult(intent, 1);
                                }
                            }
                            Intent callGPSSettingIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        }
                        Log.i("checked", "hike");
                        editor.putBoolean("hike", true);
//                        editor.putString("hike", "com.bsb.hike");
                        editor.commit();
                    }
                    else{
                        Log.i("checkednot", "hike");
                        editor.putBoolean("hike", false);
//                        editor.putString("hike", "null");
                        editor.commit();
                    }
//                }
            }
        });
        chkmessanger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(!isAccessebilityServiceEnabled()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (!Settings.canDrawOverlays(getApplicationContext())) {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                        Uri.parse("package:" + getPackageName()));
                                startActivityForResult(intent, 1);
                            }
                        }
                        Intent callGPSSettingIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                    Log.i("checked", "messanger");
                    editor.putBoolean("messanger", true);
//                    editor.putString("messanger", "com.facebook.orca");
                    editor.commit();
                }
                else{
                    Log.i("checkednot", "messanger");
                    editor.putBoolean("messanger", false);
//                    editor.putString("messanger", "null");
                    editor.commit();
                }
            }
        });
//        btn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(chkwhatsapp.isChecked()||chkhike.isChecked()||chkmessanger.isChecked()) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (!Settings.canDrawOverlays(getApplicationContext())) {
//                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                                    Uri.parse("package:" + getPackageName()));
//                            startActivityForResult(intent, 1);
//                        }
//                    }
//                    Intent callGPSSettingIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
//                    startActivity(callGPSSettingIntent);
//                }
//                else{
//                    Toast.makeText(getApplicationContext(),"Please select any one.",Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    public Boolean isAccessebilityServiceEnabled()
    {
        int accessibilityEnabled=0;
        try {
            accessibilityEnabled = Settings.Secure.getInt(this.getContentResolver(),android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.i("LOGTAG", "ACCESSIBILITY: " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.i("LOGTAG", "Error finding setting, default accessibility to not found: " + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');
        if (accessibilityEnabled==1) {
            Log.i("LOGTAG", "***ACCESSIBILIY IS ENABLED***: ");


            String settingValue = Settings.Secure.getString(getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            Log.i("LOGTAG", "Setting: " + settingValue);
            if (settingValue != null) {
                TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
                splitter.setString(settingValue);
                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();
                    Log.i("LOGTAG", "Setting: " + accessabilityService);
                    if (accessabilityService.equalsIgnoreCase(ASSECCEBILITY_SERVICE_NAME)) {
                        Log.i("LOGTAG", "We've found the correct setting - accessibility is switched on!");
                        return true;
                    } else {
                        Log.i("LOGTAG", "not found the correct setting - accessibility is switched off!");
                        return false;
                    }
                }
            }

            Log.i("LOGTAG", "***END***");
        }
        return false;
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.i("TAG", "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.i("TAG", "onPermissionsDenied:" + requestCode + ":" + perms.size());
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
