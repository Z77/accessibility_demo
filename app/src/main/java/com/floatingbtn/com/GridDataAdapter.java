package com.floatingbtn.com;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Payal on 30-Sep-16.
 */
public class GridDataAdapter extends BaseAdapter {
    private Context context;
    private List<GridDataProvide> dataprovideList;



    public GridDataAdapter(Context context, List<GridDataProvide> dataprovideList) {
        this.context = context;
        this.dataprovideList = dataprovideList;

    }
    private class ViewHolder {
        ImageView img;
        TextView title;
        TextView type;
        ProgressBar pbar;
    }

    @Override
    public int getCount() {
        return dataprovideList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataprovideList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater infla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infla.inflate(R.layout.grid_item, parent, false);
            holder = new ViewHolder();
            holder.img=(ImageView)convertView.findViewById(R.id.img_grid);
            holder.title=(TextView)convertView.findViewById(R.id.txt_title);
            holder.type=(TextView)convertView.findViewById(R.id.txt_type);
            holder.pbar=(ProgressBar)convertView.findViewById(R.id.probar);
            convertView.setTag(holder);
        } else {
            holder=(ViewHolder)convertView.getTag();
        }
        GridDataProvide data=(GridDataProvide)getItem(position);

        holder.title.setText(data.getTitle());
        holder.type.setText(data.getType());
        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(context).build();
        ImageLoader loader=ImageLoader.getInstance();
        loader.init(config);


        DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();


        // holder.img.setImageResource(data.getImage());
        loader.displayImage(data.getImg_path(), holder.img, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.pbar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        return convertView;
    }
}
