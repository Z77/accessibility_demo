package com.floatingbtn.com;

import android.graphics.Bitmap;

/**
 * Created by Payal on 30-Sep-16.
 */
public class GridDataProvide {
    String title;
    String img_path;
    String type;

    public GridDataProvide() {}

    public String getTitle() {return title;}

    public void setTitle(String title) {this.title = title;}

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

    public String getImg_path() {return img_path;}

    public void setImg_path(String img_path) {this.img_path = img_path;}
}
