package com.floatingbtn.com;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Payal on 30-Sep-16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private List<FeedItem> feedItemList;
    private Context context;
    int selectedPos = 0;

    public RecyclerAdapter(List<FeedItem> feedItemList, Context context) {
        this.feedItemList = feedItemList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_item,null);
//        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        v.setLayoutParams(lp);
        int height = parent.getMeasuredWidth()/ 3;
        v.setMinimumWidth(height);
        MyViewHolder mvh=new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.itemView.setSelected(selectedPos == position);
        if(selectedPos == position){
            // Here I am just highlighting the background
            holder.itemView.setBackgroundColor(Color.parseColor("#87ceeb"));
        }else{
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }
        FeedItem feedItem=feedItemList.get(position);
        holder.text_list.setText(feedItem.getList_string());

    }

    @Override
    public int getItemCount() {
        return feedItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView text_list;
        public MyViewHolder(View itemView) {
            super(itemView);
            text_list=(TextView)itemView.findViewById(R.id.text_list);
            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    //return false;
                    Log.i("Adapter", String.valueOf(v));
                    v.setBackgroundColor(Color.parseColor("#D3D3D3"));
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            //v.setBackgroundResource(R.drawable.background_item_event_pressed);
                            //v.setBackgroundResource(R.color.cyan);
                            Log.i("RECYCLE_Adapter","action_Down");
                            notifyItemChanged(selectedPos);
                            selectedPos = getLayoutPosition();
                            Log.i("poss", String.valueOf(selectedPos));
                            notifyItemChanged(selectedPos);
                            break;
                        case MotionEvent.ACTION_CANCEL:
                            // CANCEL triggers when you press the view for too long
                            // It prevents UP to trigger which makes the 'pressed' background permanent which isn't what we want
                            Log.i("RECYCLE_Adapter","action_cancel");
                        case MotionEvent.ACTION_OUTSIDE:
                            // OUTSIDE triggers when the user's finger moves out of the view
                            Log.i("RECYCLE_Adapter","action_outside");
                        case MotionEvent.ACTION_UP:
                            //v.setBackgroundResource(R.drawable.background_item_event);
                            Log.i("RECYCLE_Adapter","action_Up");
                            v.setBackgroundResource(R.color.cyan);
                            break;
                        default:
                            break;
                    }

                    return true;
                }
            });
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    int selecteditem=MyfabService.recyclerView.getChildPosition(v);
////                    v.setBackgroundResource(R.color.cyan);
////                    notifyItemChanged(selecteditem);
//                    notifyItemChanged(selectedPos);
//                    selectedPos = getLayoutPosition();
//                    Log.i("poss", String.valueOf(selectedPos));
//                    notifyItemChanged(selectedPos);
//                }
//            });
        }
    }

}
