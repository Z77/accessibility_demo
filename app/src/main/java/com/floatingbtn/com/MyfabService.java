package com.floatingbtn.com;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class MyfabService extends Service{

    public static final String FIREBASE_URL="https://floatingbtn-36666.firebaseio.com/b_items/0xXLYDpcHp";
    public static Boolean viewisshown=true;
    private WindowManager windowManager;
    private ImageView chatHead;
    private ImageView close;

    float prex,prey;
    int statusBarHeight,device_height,device_width;
    String packagename,ratecnt="",reviewcnt="",businessTitle="",businessImagePath,root;

    File file,pdfFile;

    String[] image_path,title,type,unit,abt,businessid;
    Boolean[] priceShown;
    Long[] price;

    //publically define settings screen layouts and params var now its not useful
    WindowManager.LayoutParams settings_screen_container;
    private LinearLayout settings_screen;
    private LinearLayout settings_screen_inner_container;

    List<FeedItem> feedItemList=new ArrayList<>();
    List<GridDataProvide> dataProvideList=new ArrayList<>();
    RecyclerAdapter adapter;
    GridView grid;
    View theInflatedView;
    WindowManager.LayoutParams params_bottomview;//for dialog_zobaze params

    public MyfabService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.i("Pack",intent.getStringExtra("Pack"));
        packagename=intent.getStringExtra("Pack");
    }
    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        final WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        Display display = window.getDefaultDisplay();
        device_width = display.getWidth();
        device_height = display.getHeight();
        Log.i("device_width-height",device_width+"  "+device_height);

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
         statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            Log.i("Status",""+statusBarHeight);
        }
        root = Environment.getExternalStorageDirectory().toString();

        chatHead=new ImageView(this);
        chatHead.setImageResource(R.mipmap.ic_zobaze_);
        close=new ImageView(this);
        close.setImageResource(R.drawable.cancel_filled);

        //final_height_of_dialog_zobaze view
        int Height=device_height/4;
        int finalHeight=device_height-Height;

        //params of zobaze icon
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,
//                110,110,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP|Gravity.LEFT;
        params.x = 0;
        params.y = 100;
        if(viewisshown)
        windowManager.addView(chatHead, params);

        LayoutInflater inflater = LayoutInflater.from(this);
        theInflatedView = inflater.inflate(R.layout.dialog_zobaze, null);

        RelativeLayout dialog=(RelativeLayout)theInflatedView.findViewById(R.id.dialog_test);
        dialog.setPadding(5,0,5,0);
        RelativeLayout cardcnt=(RelativeLayout)theInflatedView.findViewById(R.id.card_container);
        final RecyclerView tab=(RecyclerView)theInflatedView.findViewById(R.id.recyclerView);
        grid=(GridView)theInflatedView.findViewById(R.id.grid_item);
        final ImageView closee=(ImageView)theInflatedView.findViewById(R.id.icon_crosss);

        cardcnt.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight));

        LinearLayoutManager llm= new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        tab.setLayoutManager(llm);
        tab.setBackgroundColor(Color.parseColor("#90D3D3D3"));

        grid.setNumColumns(1);
        grid.setColumnWidth(GridView.AUTO_FIT);
        grid.setVerticalSpacing(10);
        grid.setLongClickable(true);

        Log.i("TAG", "above card view params");
        params_bottomview = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, finalHeight,
//                110,110,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params_bottomview.gravity=Gravity.BOTTOM;
        Log.i("TAG", "after card view params assigning process");

        //set the adapter for tab(recycler view)
        for(int i=0;i<5;i++){
            FeedItem item=new FeedItem();
            item.setList_string("Text"+i);
            feedItemList.add(item);
        }
        adapter=new RecyclerAdapter(feedItemList,this);
        tab.setAdapter(adapter);
        initTab1Data();
        grid_item_click();

        //params of close image shows when moveing the zobaze icon
        final WindowManager.LayoutParams params2 = new WindowManager.LayoutParams(
                //WindowManager.LayoutParams.WRAP_CONTENT,
                //WindowManager.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params2.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

        //class to detect zobaze icon clicked
        class SingleTapDetector extends GestureDetector.SimpleOnGestureListener {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        }

        final GestureDetector gestureDetector=new GestureDetector(this,new SingleTapDetector());
        chatHead.setOnTouchListener(new View.OnTouchListener() {

            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (gestureDetector.onTouchEvent(event)) {
                    // code for single tap or onclick
                        prex=event.getX();
                        prey=event.getY();

                        windowManager.addView(theInflatedView,params_bottomview);
                        if(theInflatedView.getParent()!=null)
                            theInflatedView.post(new Runnable() {
                                @Override
                                public void run() {
                                    circularRevealActivity(theInflatedView);
                                }
                            });

                            tab.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(),tab, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Toast.makeText(getApplicationContext(), "position:" + position, Toast.LENGTH_SHORT).show();
                                    Log.i("VIEW", String.valueOf(view));
                                    switch (position) {
                                        case 0:
                                            Toast.makeText(getApplicationContext(), "position:" + position, Toast.LENGTH_SHORT).show();
                                            initTab1Data();
                                            grid_item_click();
                                            break;

                                        case 1:
                                            Toast.makeText(getApplicationContext(), "position:" + position, Toast.LENGTH_SHORT).show();
                                            initTab2Data();
                                            grid_item_click();
                                            break;
                                    }
                                }
                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            }));
                        closee.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(theInflatedView.getParent()!=null){
                                    windowManager.removeView(theInflatedView);
                                    chatHead.setVisibility(View.VISIBLE);
                                }
                            }
                        });
//                            Toast.makeText(getApplicationContext(), "Hello, Zobaze", Toast.LENGTH_SHORT).show();
                    windowManager.removeView(close);
                }else{
                    switch (event.getAction()) {

                        case MotionEvent.ACTION_DOWN:
                            initialX = params.x;
                            initialY = params.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            Log.i("action", "action_down");

                            if(event.getRawX()>initialX&&event.getRawY()>initialY){
                                windowManager.addView(close, params2);
                            }
                            //windowManager.addView(close, params2);
                            return true;

                        case MotionEvent.ACTION_UP:
                            windowManager.removeView(close);

                            int center_Width=device_width/2;
                            int sorroundara=close.getMeasuredWidth();//close btn width

                            int area_h_left=center_Width-sorroundara;//surrounding left side area of close btn
                            int area_h_right=center_Width+sorroundara;//surrounding right side area of close btn
                            Log.i("params","area_H l="+area_h_left+"r="+area_h_right);
                            Log.i("params1","chathe"+chatHead.getMeasuredHeight()+"closehe"+close.getMeasuredHeight());

                            int area_v_top=(device_height-(statusBarHeight+chatHead.getMeasuredHeight()+close.getMeasuredHeight()));
                            Log.i("params","area_V"+area_v_top);

                            if((params.x>=area_h_left && params.x<=area_h_right)&&params.y>=area_v_top){
                                Log.i("params","action :layout on close");
                                viewisshown=!viewisshown;
                                windowManager.removeView(chatHead);
                            }
                            else{
                                //windowManager.addView(close,params2);
                                windowManager.removeView(close);
                            }

                            Log.i("action","action_up");
                            return true;

                        case MotionEvent.ACTION_MOVE:
                            Log.i("action", "action_move");

                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);

                            windowManager.updateViewLayout(chatHead, params);

                            int x=params.x;int y=params.y;
                            Log.i("paramsA","X="+x+"Y="+y);

                            int center_Width_move=device_width/2;
                            int sorroundara_move=close.getMeasuredWidth();

                            int area_h_left_move=center_Width_move-sorroundara_move;
                            int area_h_right_move=center_Width_move+sorroundara_move;
                            int measuredwidth=close.getMeasuredWidth();
                            Log.i("measure",measuredwidth+"");

                            Log.i("params2", "chathe" + chatHead.getMeasuredHeight() + "closehe" + close.getMeasuredHeight());

                            int area_v_top_move=(device_height-(statusBarHeight+chatHead.getMeasuredHeight()+close.getMeasuredHeight()));

                            if((params.x>=area_h_left_move && params.x<=area_h_right_move)&&params.y>=area_v_top_move) {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                                ObjectAnimator scaleX = ObjectAnimator.ofFloat(close, close.SCALE_X, 1.5f);
                                    scaleX.setInterpolator(new OvershootInterpolator());
                                ObjectAnimator scaleY = ObjectAnimator.ofFloat(close, close.SCALE_Y, 1.5f);
                                    scaleY.setInterpolator(new OvershootInterpolator());
                                    scaleX.setDuration(50);
                                    scaleY.setDuration(50);
                                scaleX.start();
                                Log.i("XS", "scale X started");
                                scaleY.start();
                                Log.i("YS", "scale Y started");
//                                close.requestLayout();
                                }else{
                                    final WindowManager.LayoutParams params2_new = new WindowManager.LayoutParams(150,150,
                                            WindowManager.LayoutParams.TYPE_PHONE,
                                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                                            PixelFormat.TRANSLUCENT);
                                    params2_new.gravity=Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL;
                                    windowManager.updateViewLayout(close,params2_new);
                                }
                            }
                            else{
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                                    ObjectAnimator scaleX = ObjectAnimator.ofFloat(close, close.SCALE_X, 1f);
                                    ObjectAnimator scaleY = ObjectAnimator.ofFloat(close, close.SCALE_Y, 1f);
                                    scaleX.setDuration(50);
                                    scaleY.setDuration(50);
                                    scaleX.start();
                                    scaleY.start();
                                }else{
                                windowManager.updateViewLayout(close,params2);}
                            }
                            return true;
                        }
                    }
                    //return false;
                    return true;
            }
        });
    }
    public void initTab1Data(){
        if(dataProvideList.size()>0)
            dataProvideList.clear();

        Firebase firebase = new Firebase(FIREBASE_URL);
        firebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String data = String.valueOf(dataSnapshot.getChildrenCount());
                Log.i("DATASNAP", String.valueOf(dataSnapshot));
                HashMap<String, Object> business = (HashMap<String, Object>) dataSnapshot.getValue();
                image_path = new String[(int) dataSnapshot.getChildrenCount()];
                title = new String[(int) dataSnapshot.getChildrenCount()];
                type = new String[(int) dataSnapshot.getChildrenCount()];
                priceShown = new Boolean[(int) dataSnapshot.getChildrenCount()];
                price = new Long[(int) dataSnapshot.getChildrenCount()];
                unit = new String[(int) dataSnapshot.getChildrenCount()];
                abt = new String[(int) dataSnapshot.getChildrenCount()];
                businessid = new String[(int) dataSnapshot.getChildrenCount()];

                int i = 0;
                for (Object obj : business.values()) {
                    HashMap<String, Object> businessmap = (HashMap<String, Object>) obj;
                    //image_path[i]=(String)businessmap.remove("picture");
                    image_path[i] = (String) businessmap.remove("image");
                    title[i] = (String) businessmap.remove("title");
                    type[i] = (String) businessmap.remove("type");
                    priceShown[i] = (Boolean) businessmap.remove("showprice");
//                    if(businessmap.remove("showprice")!=null){
//                        priceShown[i] = (Boolean) businessmap.remove("showprice");
//                        Log.i("price","price is showing"+priceShown[i]);
//                        Log.i("price","price is showing");
//                    }
//                    else{
//                        Log.i("price","price is nt showing");
//                        //Log.i("price","price is showing"+businessmap.remove("showprice"));
//                        priceShown[i] =Boolean.FALSE;
//                        Log.i("price","price is showing"+priceShown[i]);
//                    }
//                    if(businessmap.remove("price")!=null){
                        price[i] = (Long) businessmap.remove("price");
//                    }else {
//                        price[i] = Long.valueOf("");
//                    }
//
//                    if(businessmap.remove("unit")!=null){
                        unit[i] = (String) businessmap.remove("unit");
//                    }else{
//                        unit[i] ="";
//                    }
                    abt[i] = (String) businessmap.remove("about");
                    businessid[i] = (String) businessmap.remove("businessId");

                    GridDataProvide adapter = new GridDataProvide();
                    adapter.setImg_path(image_path[i]);
                    adapter.setTitle(title[i]);
                    adapter.setType(type[i]);
                    dataProvideList.add(adapter);
                    i++;

                }
                GridDataAdapter adapter = new GridDataAdapter(getApplicationContext(), dataProvideList);
                grid.setAdapter(adapter);
                grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void initTab2Data(){
        if(dataProvideList.size()>0)
            dataProvideList.clear();

        Firebase firebase = new Firebase("https://floatingbtn-36666.firebaseio.com/b_items/yvIhilRvUy");
        firebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String data = String.valueOf(dataSnapshot.getChildrenCount());
                Log.i("DATASNAP", String.valueOf(dataSnapshot));
                HashMap<String, Object> business = (HashMap<String, Object>) dataSnapshot.getValue();
                image_path = new String[(int) dataSnapshot.getChildrenCount()];
                title = new String[(int) dataSnapshot.getChildrenCount()];
                type = new String[(int) dataSnapshot.getChildrenCount()];
                priceShown = new Boolean[(int) dataSnapshot.getChildrenCount()];
                price = new Long[(int) dataSnapshot.getChildrenCount()];
                unit = new String[(int) dataSnapshot.getChildrenCount()];
                abt = new String[(int) dataSnapshot.getChildrenCount()];
                businessid = new String[(int) dataSnapshot.getChildrenCount()];

                int i = 0;
                for (Object obj : business.values()) {
                    HashMap<String, Object> businessmap = (HashMap<String, Object>) obj;
                    //image_path[i]=(String)businessmap.remove("picture");
                    image_path[i] = (String) businessmap.remove("image");
                    title[i] = (String) businessmap.remove("title");
                    type[i] = (String) businessmap.remove("type");
                    priceShown[i] = (Boolean) businessmap.remove("showprice");
//                    if(businessmap.remove("showprice")!=null){
//                        priceShown[i] = (Boolean) businessmap.remove("showprice");
//                        Log.i("price","price is showing"+priceShown[i]);
//                        Log.i("price","price is showing");
//                    }
//                    else{
//                        Log.i("price","price is nt showing");
//                        //Log.i("price","price is showing"+businessmap.remove("showprice"));
//                        priceShown[i] =false;
//                        Log.i("price","price is showing"+priceShown[i]);
//                    }
//                    if(businessmap.remove("price")!=null){
                        price[i] = (Long) businessmap.remove("price");
//                    }else {
//                        price[i] = (Long) businessmap.remove("");
//                    }
//
//                    if(businessmap.remove("unit")!=null){
                        unit[i] = (String) businessmap.remove("unit");
//                    }else{
//                        unit[i] ="";
//                    }
                    abt[i] = (String) businessmap.remove("about");
                    businessid[i] = (String) businessmap.remove("businessId");

                    GridDataProvide adapter = new GridDataProvide();
                    adapter.setImg_path(image_path[i]);
                    adapter.setTitle(title[i]);
                    adapter.setType(type[i]);
                    dataProvideList.add(adapter);
                    i++;
                }
                GridDataAdapter adapter = new GridDataAdapter(getApplicationContext(), dataProvideList);
                grid.setAdapter(adapter);
                grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    private void SaveImage(Bitmap finalBitmap,String name) {

        File myDir = new File(root + "/Zobaze_images");
        myDir.mkdirs();
        String fname = name;
        file = new File (myDir, fname);
        if (file.exists ()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {e.printStackTrace();}
    }
    public void grid_item_click(){
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                chatHead.setVisibility(View.VISIBLE);
                windowManager.removeView(theInflatedView);

                Toast.makeText(getApplicationContext(), "Wait...", Toast.LENGTH_SHORT).show();
                ImageView siv = (ImageView) view.findViewById(R.id.img_grid);
                BitmapDrawable bitmapDrawable = (BitmapDrawable) siv.getDrawable();
                Bitmap bitmap = bitmapDrawable.getBitmap();
                String path = com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme.FILE.wrap(image_path[position]);
                String[] parts = path.split("image%");
                Log.i("parts", String.valueOf(parts[1]));
                final String[] name = parts[1].split("\\?");
                Log.i("parts", String.valueOf(name[0]));

                if (EasyPermissions.hasPermissions(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    SaveImage(bitmap, name[0]);//function which stores image in sdcard
                    try {
                        Firebase firebase = new Firebase("https://floatingbtn-36666.firebaseio.com/short/business/"+businessid[position]);
                        firebase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                //data fatching is not completed
                                String data = String.valueOf(dataSnapshot.getChildrenCount());
                                Log.i("DATASNAP", String.valueOf(dataSnapshot));
                                HashMap<String, Object> business = (HashMap<String, Object>) dataSnapshot.getValue();
                                Log.i("DATA", "" + business);
                                businessImagePath=(String)business.get("picture");
                                businessTitle= (String) business.get("title");
                                HashMap rating= (HashMap) business.get("rating");
                                Log.i("TAG","rating"+rating);
                                ratecnt= String.valueOf((Long) rating.get("rate_star"));
                                reviewcnt= String.valueOf((Long) rating.get("reviews_count"));

                                cratePDF(name[0], title[position], priceShown[position], price[position], unit[position], abt[position],businessImagePath,businessTitle, ratecnt, reviewcnt);
                            }
                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
//                        cratePDF(name[0], title[position], priceShown[position], price[position], unit[position], abt[position],ratecnt,reviewcnt);
                    } catch (Exception e) {Log.i("TAG", "" + e);}
                } else {
//                        EasyPermissions.requestPermissions(getApplicationContext(),"Request for the use of External storage.",
//                                RC_WRITE_EXTERNAL_STORAGE_PERM, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    Toast.makeText(getApplication(), "Make sure have permission to WRITE_EXTERNAL_STORAGE in app settings", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void addSettingsScreenView(){
        int Height=device_height/4;
        int finalHeight=device_height-Height-100; //100 is the hight of text_image_container
        //Params of card_container holds relative layout
        settings_screen_container=new WindowManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        settings_screen_container.gravity=Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL;

        final LinearLayout.LayoutParams settings_screen_inner_container_para=new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        final  LinearLayout.LayoutParams switch_params=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        settings_screen.setOrientation(LinearLayout.VERTICAL);
        settings_screen.setPadding(25, 25, 25, 0);

        settings_screen_inner_container.setOrientation(LinearLayout.VERTICAL);
        settings_screen_inner_container.setBackgroundColor(Color.WHITE);

        Switch switch_new=new Switch(this);
        switch_params.gravity=Gravity.CENTER_HORIZONTAL;

        windowManager.addView(settings_screen, settings_screen_container);
        settings_screen.addView(settings_screen_inner_container,settings_screen_inner_container_para);
        settings_screen_inner_container.addView(switch_new, switch_params);
        switch_new.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String ischeck;
                if (isChecked) {
                    ischeck = "On";
                } else {
                    ischeck = "Off";
                }
                Toast.makeText(getApplicationContext(), "switch is :" + ischeck, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void removeSettingsScreenView(){
        if(settings_screen.getParent()!=null){

            settings_screen_inner_container.removeAllViews();
            settings_screen.removeAllViews();
            windowManager.removeView(settings_screen);

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (chatHead != null)
        if(viewisshown)
            windowManager.removeView(chatHead);


        if(theInflatedView.getParent()!=null){
            windowManager.removeView(theInflatedView);
        }

        viewisshown=!viewisshown;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            Toast.makeText(this, "landscape="+Height, Toast.LENGTH_SHORT).show();

//            Log.i("parent1", String.valueOf(cardContainer.getParent()));
            if(theInflatedView.getParent()!=null){
                Log.i("card","card container has parent");
                final WindowManager window = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = window.getDefaultDisplay();
                final int device_height = display.getHeight();
                int Height=device_height/4;
                int finalHeight=device_height-Height;
                //Params of card_container holds relative layout
                final WindowManager.LayoutParams card_params_new=new
                        WindowManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, finalHeight,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                card_params_new.gravity=Gravity.BOTTOM;
                windowManager.updateViewLayout(theInflatedView, card_params_new);
            }
        }
        else{
            if(theInflatedView.getParent()!=null){
            windowManager.updateViewLayout(theInflatedView,params_bottomview);
            }
        }
    }
    private void circularRevealActivity(View rootLayout) {

        int cx = rootLayout.getWidth() / 2;
        int cy = rootLayout.getHeight() / 2;
        Log.i("card","x="+cx+",y="+cy);
        float finalRadius = Math.max(cx,cy);

        // create the animator for this view (the start radius is zero)
        Animator circularReveal;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, cx, cy, 0, finalRadius);
            circularReveal.setDuration(300);
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        }else{
            rootLayout.setVisibility(View.VISIBLE);
        }
        //circularReveal.setDuration(1000);

        // make the view visible and start the animation
//        rootLayout.setVisibility(View.VISIBLE);
//        circularReveal.start();

    }
    //here only assigning the value to the view
    public void cratePDF(String imgFilenm,String title,Boolean pricelayout_shown,Long price,String unit,String abt_info,String business_Image, final String business_Title,String rateCnt,String reviewCnt){

        //assigning value from para to particular view
        LayoutInflater inflater = LayoutInflater.from(this);
        final View pdfview=inflater.inflate(R.layout.item_template, null,false);

        ScrollView scrollView=(ScrollView)pdfview.findViewById(R.id.scroll1);
        NestedScrollView nestedScrollView=(NestedScrollView)pdfview.findViewById(R.id.nestedScroll1);
        final ImageView topbarimg=(ImageView)pdfview.findViewById(R.id.topImg);
        topbarimg.setImageURI(Uri.parse(root + "/Zobaze_images/" + imgFilenm));
        final LinearLayout mainParent=(LinearLayout)pdfview.findViewById(R.id.mainParent);


        TextView productTitle=(TextView)pdfview.findViewById(R.id.producttitle);
        TextView priceperunit=(TextView)pdfview.findViewById(R.id.priceperunit);
        TextView abt=(TextView)pdfview.findViewById(R.id.about);
        TextView businessTitle=(TextView)pdfview.findViewById(R.id.businesstitle);
        RatingBar stars=(RatingBar)pdfview.findViewById(R.id.rating);
        Drawable drawable = stars.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#FFC611"), PorterDuff.Mode.SRC_ATOP);
        TextView reviewcnt=(TextView)pdfview.findViewById(R.id.review_count);

        productTitle.setText(title);
        if(pricelayout_shown) {
                if(unit!=null){
                priceperunit.setText("Rs "+price+" / "+unit);
                }else{
                priceperunit.setText("Rs "+price);
                }
        }else{
            priceperunit.setVisibility(View.GONE);
        }
        abt.setText(abt_info);
        businessTitle.setText(business_Title);
        stars.setRating(Float.parseFloat(rateCnt));
        reviewcnt.setText("(" + reviewCnt + ")");

        scrollView.setLayoutParams(new LinearLayout.LayoutParams(device_width, ViewGroup.LayoutParams.WRAP_CONTENT));
        Log.i("TAG","scrollview W="+scrollView.getWidth() + " H" + scrollView.getHeight());
        Log.i("TAG","scrollview measuredW="+ scrollView.getMeasuredWidth() + "H" + scrollView.getMeasuredHeight());

        nestedScrollView.setLayoutParams(new LinearLayout.LayoutParams(device_width, ViewGroup.LayoutParams.WRAP_CONTENT));
        Log.i("PAYAL","nested scrollview W="+ nestedScrollView.getWidth() + "H" + nestedScrollView.getHeight());
        Log.i("PAYAL","nested scrollview measuredW="+ nestedScrollView.getMeasuredWidth() + " H" + nestedScrollView.getMeasuredHeight());

//        mainParent.post(new Runnable() {
//            @Override
//            public void run() {
//                Log.i("TAGI", "Scrollview get" + mainParent.getWidth() + "H" + mainParent.getHeight());
//                Log.i("TAGI", "Scrollview measure" + mainParent.getMeasuredWidth() + "H" + mainParent.getMeasuredHeight());
//            }
//        });

        Log.i("TAG4", "w " + pdfview.getWidth() + "h " + pdfview.getHeight());
        pdfview.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Log.i("TAG4", "w " + pdfview.getMeasuredWidth() + "h " + pdfview.getMeasuredHeight());


        ImageLoaderConfiguration config=new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader loader=ImageLoader.getInstance();
        loader.init(config);
        DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true).resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
        loader.displayImage(business_Image, (ImageView)pdfview.findViewById(R.id.businessimage), options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Toast.makeText(getApplicationContext(), "Wait...", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                bitmap_pdfview = Bitmap.createBitmap(pdfview.getMeasuredWidth(), pdfview.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//                Log.i("TAG5","bitmapcreated");
                saveScreenShot(pdfview,mainParent,business_Title);//taking the screen shot from the
                Log.i("TAG6", "canvas ready");
            }
            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

    //saving the screen shot of item_template.xml's parent view(root view) and generating the pdf view
    public void saveScreenShot(View pdfview,View mainParent,String business_Title){
        int width_=pdfview.getMeasuredWidth();
        int height_=pdfview.getMeasuredHeight();
        mainParent.layout(0, 0, width_,height_);//most important without this mainParent.draw(comboImage); does not work
        Log.i("TAG","Pdf view:"+width_+""+height_);

        //take screenshot of full view
        Bitmap cs=Bitmap.createBitmap(width_,height_, Bitmap.Config.ARGB_8888);//generate the bitmap as per our parent view height and width
        Canvas comboImage = new Canvas(cs);
        comboImage.drawColor(Color.parseColor("#F2F2F2"));
        comboImage.save();
        mainParent.draw(comboImage);
        comboImage.restore();
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        //to draw "created on" and "powered by" text on the canvas (comboImage)
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        paint.setTextSize(16);

        Rect rectText = new Rect();
        String creted="Created on:" + now.toString().replace(" ", "");
        paint.getTextBounds(creted, 0, creted.length(), rectText);
        comboImage.drawText(creted, 0, (mainParent.getMeasuredHeight() - 10), paint);
        String zobaze="Powered By:Zobaze";
        paint.getTextBounds(zobaze, 0, zobaze.length(), rectText);
        int widthText = rectText.left + rectText.width()+10;
        comboImage.drawText(zobaze,(device_width-widthText),(mainParent.getMeasuredHeight()- 10), paint);

        try {
            //save that full imagefile
            File imageFile = new File(root + "/Zobaze_images/" + "newpdffile.png");
            if (imageFile.exists()) imageFile.delete();
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            Log.i("TAG7", "new full image file created");
            int quality = 100;
            cs.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //create pdf from above imageFile
            Rectangle pagesize = new Rectangle(width_,height_);//topbarimg.getMeasuredWidth(),bitmap_pdfview.getHeight());
            Document document=new Document(pagesize,0,0,0,0);
            //create the pdf file
            PdfWriter.getInstance(document, new FileOutputStream(root + "/Zobaze_images" + "/"+business_Title+".pdf"));
            pdfFile = new File(root + "/Zobaze_images" + "/"+business_Title+".pdf");
            document.open();
            Image im= Image.getInstance(root + "/Zobaze_images" + "/" + imageFile.getName());
            document.add(im);
            Log.i("TAG8", "image written");
            document.close();
            Toast.makeText(getApplicationContext(),"PDF Saved",Toast.LENGTH_SHORT).show();

            ShareVia();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void ShareVia(){
        try{

//            Uri image_Uri = Uri.parse(file.getAbsolutePath());
//            Log.i("imageuri", String.valueOf(image_Uri));
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setPackage(packagename);

            //Depending on the package nm add text and then Image URI
            if (!packagename.contains("com.facebook.orca")) {
                shareIntent.putExtra(Intent.EXTRA_TEXT, "www.google.com");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(pdfFile));
                //shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
            } else {
                shareIntent.putExtra(Intent.EXTRA_TEXT, "www.google.com");
            }
            shareIntent.setType("application/pdf");
            //shareIntent.setType("image/jpeg");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                startActivity(shareIntent);
            } catch (ActivityNotFoundException ex) {
                //ToastHelper.MakeShortText("Whatsapp have not been installed.");
                Toast.makeText(getApplication(), "Please try again or later", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            Log.i("TAG9", ""+e.getMessage());
            Toast.makeText(getApplicationContext(),"img file not found",Toast.LENGTH_SHORT).show();
        }
    }

}

