package com.floatingbtn.com;

import android.*;
import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.EventLog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;

import static android.support.v4.app.ActivityCompat.requestPermissions;

/**
 * Created by Payal on 26-Sep-16.
 */
public class FloatbuttonService extends AccessibilityService  {
    public static String TAG;
    private static final int RC_WRITE_EXTERNAL_STORAGE_PERM = 123;
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i("FLOATSERVICE1","onAccessibilityservice");
        Log.i("FLOATSERVICE2", String.valueOf(event.getText()));
        String appname=String.valueOf(event.getText());
        Log.i("TAG","appname="+appname);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        boolean whatsapp= pref.getBoolean("whatsapp", false);
        boolean hike=pref.getBoolean("hike",false);
        boolean messanger=pref.getBoolean("messanger",false);
        Log.i("bools", "what=" + whatsapp + "," + "hike=" + hike+","+"messanger="+messanger);
//        if(appname.contains("WhatsApp")&&whatsapp){
//            Log.i("TAG","whatsappOpen");
//            startService(new Intent(this, MyfabService.class));
//        }
//        else if(appname.contains("hike")&&hike){
//            Log.i("TAG","hikeappOpen");
//            startService(new Intent(this, MyfabService.class));
//
//        }
//        else if(appname.contains("Messenger")&&messanger){
//            Log.i("TAG","messangerOpen");
//            startService(new Intent(this, MyfabService.class));
//        }
//        if(appname.contains("Overview")||appname.contains("Home")){
//            Log.i("TAG","recentApp");
//            stopService(new Intent(this, MyfabService.class));
//        }
        String packagename= String.valueOf(event.getPackageName());
        Log.i("TAG","packagename=" +String.valueOf(event.getPackageName()));
//        if(!packagename.contains("whatsapp")){
//            Log.i("TAG","whats app out");
//            stopService(new Intent(this, MyfabService.class));
//        }
//        if(!event.getPackageName().toString().contains("hike")){
//            Log.i("TAG","hike app out");
//            stopService(new Intent(this, MyfabService.class));
//        }
//        if(!event.getPackageName().toString().contains("facebook")){
//            Log.i("TAG","messanger app out");
//            stopService(new Intent(this, MyfabService.class));
//        }
        String eventnm =event.getPackageName().toString();
        Log.i(TAG,"eventname="+eventnm);

        switch(packagename){
            case "com.whatsapp":
                if(whatsapp){
                    Intent i=new Intent(this, MyfabService.class);
                    i.putExtra("Pack",packagename);
                startService(i);
                }
                break;
            case "com.bsb.hike":
                if(hike){
                    Intent i=new Intent(this, MyfabService.class);
                    i.putExtra("Pack",packagename);
                startService(i);}
                break;
            case "com.facebook.orca":
                if(messanger){
                    Intent i=new Intent(this, MyfabService.class);
                    i.putExtra("Pack",packagename);
                startService(i);}
                break;

                default:
                    stopService(new Intent(this, MyfabService.class));

        }



//        else {
//            Log.i("DATAA","noting open");
//        }




//        String whats=event.getText().toString();
//        Log.i(TAG,whats);
//        if(event.getText().toString().equals("")){
//            stopService(new Intent(this,MyfabService.class));
//        }
//        String sn=event.getClassName().notify();
//        Log.i("notify", String.valueOf());
        int s=event.getAction();
        Log.i(TAG,"getAction()"+s+"==="+event);

//        String eventnm =event.getPackageName().toString();
//        Log.i(TAG,"eventname="+ String.valueOf(eventnm));
    }

    @Override
    public void onInterrupt() {
        Log.i("FLOATSERVICE", "onInterrupt");
        stopService(new Intent(this, MyfabService.class));

    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i("SHUTDOWN","shutdown");
        stopService(new Intent(this, MyfabService.class));
        return super.onUnbind(intent);
    }

    @Override
    protected void onServiceConnected() {
        //super.onServiceConnected();
        Log.i("FLOATSERVICE4","onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            info.eventTypes = AccessibilityEvent.TYPE_WINDOWS_CHANGED;
//            info.feedbackType = AccessibilityEvent.TYPE_WINDOWS_CHANGED;
//        }else {
            info.eventTypes = AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
            info.feedbackType = AccessibilityEvent.TYPES_ALL_MASK;
//        }
        //info.feedbackType = AccessibilityEvent.TYPES_ALL_MASK;

        //info.packageNames=new String[]{"com.whatsapp","com.bsb.hike","com.facebook.orca"};
       // info.flags=AccessibilityServiceInfo.FLAG_REQUEST_FILTER_KEY_EVENTS;
        //info.flags=AccessibilityServiceInfo.CAPABILITY_CAN_REQUEST_TOUCH_EXPLORATION;
        //info.flags = AccessibilityServiceInfo.FLAG_REQUEST_TOUCH_EXPLORATION_MODE;

        setServiceInfo(info);

        Toast.makeText(getApplicationContext(),"ServiceConnected",Toast.LENGTH_LONG).show();
    }
//    private void checkFilePermissions() {
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
////            int hasWriteExternalStoragePermission = checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
////            if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
////                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},123);
////            }
////        }
//        int permission = ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (permission != PackageManager.PERMISSION_GRANTED) {
//            // We don't have permission so prompt the user
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                        RC_WRITE_EXTERNAL_STORAGE_PERM);
//            }//Toast.makeText(getApplicationContext(),"Toast for Permission",Toast.LENGTH_LONG).show();
//        }
//    }

    @Override
    protected boolean onGesture(int gestureId) {
        Log.i("gesture2", String.valueOf(gestureId));
        if(gestureId == GLOBAL_ACTION_HOME) {
            Log.i("TAG", "Gesture Home Pressed");
        }
        //return true;
        return super.onGesture(gestureId);
    }


    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        Log.i("keycodeee", String.valueOf(keyCode));
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                Log.i("keycode", "Back");
                stopService(new Intent(this,MyfabService.class));
                return false;

            case KeyEvent.KEYCODE_HOME:
                Log.i("keycode", "Home");
                stopService(new Intent(this, MyfabService.class));
                return false;
            case KeyEvent.KEYCODE_APP_SWITCH:
                Log.i("keycode", "AppSwitch");
                stopService(new Intent(this, MyfabService.class));
                return false;

        }
        return true;
        //return super.onKeyEvent(event);
    }

//    @Override
//    public AccessibilityNodeInfo getRootInActiveWindow() {
//        Log.i("getroot","getRootInActiveWindow");
//        return super.getRootInActiveWindow();
//
//    }
}
